<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app = new Silex\Application();
$app->register(new DerAlex\Silex\YamlConfigServiceProvider('../app/config.yml'));
$app->register(new DoctrineServiceProvider(), array(
    'db.options' => [
        'driver' => $app['config']['database']['driver'],
        'host' => $app['config']['database']['host'],
        'dbname' => $app['config']['database']['dbname'],
        'user' => $app['config']['database']['user'],
        'password' => $app['config']['database']['password'],
        'charset' => $app['config']['database']['charset'],
    ],
));
$app->register(new TwigServiceProvider(), [
    'twig.path' => __DIR__ . $app['config']['twig']['path'],
]);
$app['debug'] = $app['config']['debug'];

$app->get('/', function() use ($app) {
    (new Cryptoarb\Controller\MainController($app['db']))->updateAll();
    return $app['twig']->render('index.twig', array(
       'output' => (new Cryptoarb\Controller\MainController($app['db']))->findTrades()
       ));
});

$app->run();
