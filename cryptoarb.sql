-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 09 Maj 2016, 01:09
-- Wersja serwera: 5.5.49-0ubuntu0.14.04.1
-- Wersja PHP: 5.6.21-1+donate.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `exchanges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `markets_url` varchar(255) DEFAULT NULL,
  `orderbook_url` varchar(255) DEFAULT NULL,
  `json` longtext,
  `ignore_coins` text,
  `synch_time` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

INSERT INTO `exchanges` (`id`, `name`, `markets_url`, `orderbook_url`, `json`, `ignore_coins`, `synch_time`, `active`) VALUES
(1, 'Bittrex', 'https://bittrex.com/api/v1.1/public/getmarkets', 'https://bittrex.com/api/v1.1/public/getorderbook?type=both&depth=5&market=', NULL, NULL, '2016-05-05 17:08:24', 1),
(2, 'Poloniex', 'https://poloniex.com/public?command=returnTicker', 'https://poloniex.com/public?command=returnOrderBook&depth=5&currencyPair=', NULL, NULL, '2016-05-05 17:08:27', 1),
(3, 'Bter', 'http://data.bter.com/api/1/pairs', 'http://data.bter.com/api/1/depth/', NULL, '["BTS","DTC","SFR"]', '2016-05-05 17:08:30', 1),
(4, 'Bleutrade', 'https://bleutrade.com/api/v2/public/getmarkets', 'https://bleutrade.com/api/v2/public/getorderbook?type=all&depth=5&market=', NULL, NULL, '2016-05-05 17:08:33', 1),
(5, 'Banx', 'https://www.banx.io/SimpleAPI?a=markets', 'https://www.banx.io/SimpleAPI?a=orderbook&c=$1&p=$2', NULL, NULL, '2016-01-01 14:18:31', 0);

ALTER TABLE `exchanges`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

CREATE TABLE IF NOT EXISTS `coin_pairs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exchange` int(10) unsigned NOT NULL,
  `base_coin` varchar(8) NOT NULL,
  `market_coin` varchar(8) NOT NULL,
  `buy_average_rate` float(16,8) unsigned NOT NULL,
  `buy_volume` float(16,8) unsigned NOT NULL,
  `sell_average_rate` float(16,8) unsigned NOT NULL,
  `sell_volume` float(16,8) unsigned NOT NULL,
  `synch_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `exchange_pair` (`exchange`,`base_coin`,`market_coin`),
  KEY `exchange` (`exchange`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=748 ;

ALTER TABLE `coin_pairs`
  ADD CONSTRAINT `coin_pairs_ibfk_1` FOREIGN KEY (`exchange`) REFERENCES `exchanges` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
