<?php

namespace Cryptoarb;

interface ExchangeApiInterface {

    const
        LIMIT = 3,
        MAX_CONNECTIONS = 25;

    public function __construct($db);

    public function updateExchange(array $exchange);
}
