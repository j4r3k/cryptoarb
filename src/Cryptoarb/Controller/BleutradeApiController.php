<?php

namespace Cryptoarb\Controller;

use Cryptoarb\Controller\ExchangeApiController;

class BleutradeApiController extends ExchangeApiController
{

    protected
        $separator = '_',
        $urlMarketsVar = 'market';

    protected function isJsonValid($json)
    {
        return ($json->success === 'true' and is_array($json->result)) ? true : false;
    }

    protected function setMarkets($json)
    {
        $ignore = $this->getIgnoreCoins();
        foreach ($json->result as $market) {
            if ($market->IsActive === 'true' and strpos(strtoupper($market->MarketName), 'BTC') !== false
                and ($ignore === null or ($ignore !== null and preg_match($ignore, $marketName) === 0))) {
                $market->marketName = $market->MarketName;
                $this->markets[] = $market;
            }
        }
    }

    protected function getBuyData($json)
    {
        $i = $buyTotalValue = $buyVolume = $buyAverageRate = 0;
        if ($json->success === 'true' and ! empty($json->result->buy)) {
            foreach ($json->result->buy as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $buyTotalValue += round($order->Quantity * $order->Rate, 8);
                $buyVolume += $order->Quantity;
                $i++;
            }
            if ($buyVolume > 0) {
                $buyAverageRate = round($buyTotalValue / $buyVolume, 8);
            }
        }

        return [
            'buyAverageRate' => $buyAverageRate,
            'buyVolume' => $buyVolume,
        ];
    }

    protected function getSellData($json)
    {
        $i = $sellTotalValue = $sellVolume = $sellAverageRate = 0;
        if ($json->success === 'true' and ! empty($json->result->sell)) {
            foreach ($json->result->sell as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $sellTotalValue += round($order->Quantity * $order->Rate, 8);
                $sellVolume += $order->Quantity;
                $i++;
            }
            if ($sellVolume > 0) {
                $sellAverageRate = round($sellTotalValue / $sellVolume, 8);
            }
        }

        return [
            'sellAverageRate' => $sellAverageRate,
            'sellVolume' => $sellVolume,
        ];
    }
}
