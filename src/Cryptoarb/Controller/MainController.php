<?php

namespace Cryptoarb\Controller;

use Cryptoarb\Factory\ExchangeApiFactory;

class MainController
{

    const PROFIT_MARGIN = 1.05;

    private
        $db,
        $qb;

    public function __construct($db)
    {
        $this->db = $db;
        $this->qb = $this->db->createQueryBuilder();
    }

    public function updateAll()
    {
        foreach ($this->getExchanges() as $exchange) {
            $api = ExchangeApiFactory::build($this->db, $exchange['name']);
            $api->updateExchange($exchange);
        }
    }

    public function findTrades()
    {
        $query = "SELECT exchanges.name AS exchange, pair1 AS pair, buy_average_rate, buy_volume, sell_average_rate, sell_volume "
            . "FROM "
            . "((SELECT exchange, CONCAT_WS('-', base_coin, market_coin ) AS pair1, buy_average_rate, buy_volume, sell_average_rate, sell_volume "
            . "FROM coin_pairs WHERE UNIX_TIMESTAMP(synch_time) > UNIX_TIMESTAMP() - 300) t1, "
            . "(SELECT CONCAT_WS( '-', base_coin, market_coin ) AS pair2 "
            . "FROM coin_pairs GROUP BY pair2 HAVING COUNT(exchange) > 1) t2) "
            . "JOIN exchanges ON exchanges.id = t1.exchange WHERE t1.pair1 = t2.pair2 ORDER BY pair1";
        $markets = $this->db->fetchAll($query);
        $this->restructureMarkets($markets);

        return $this->findOptions($markets);
    }

    private function getExchanges()
    {
        $query = $this->qb
            ->select('*')
            ->from('exchanges')
            ->where('active = 1')
        ;

        return $this->db->fetchAll($query);
    }

    private function restructureMarkets(&$markets) {
        $temp = [];
        foreach ($markets as $market) {
            $temp[$market['pair']][] = [
                'exchange' => $market['exchange'],
                'buy_average_rate' => $market['buy_average_rate'],
                'buy_volume' => $market['buy_volume'],
                'sell_average_rate' => $market['sell_average_rate'],
                'sell_volume' => $market['sell_volume'],
            ];
        }
        $markets = $temp;
    }

    private function findOptions(&$markets) {
        $options = [];
        foreach ($markets as $pair => $market) {
            $maxBuy['rate'] = 0;
            $minSell['rate'] = PHP_INT_MAX;
            foreach ($market as $exchange) {
                if ($exchange['buy_average_rate'] > $maxBuy['rate']) {
                    $maxBuy = [
                        'exchange' => $exchange['exchange'],
                        'rate' => $exchange['buy_average_rate'],
                        'volume' => $exchange['buy_volume'],
                        'btc_value' => $exchange['buy_average_rate'] * $exchange['buy_volume'],
                        ];
                }
                if ($exchange['sell_average_rate'] < $minSell['rate']) {
                    $minSell = [
                        'exchange' => $exchange['exchange'],
                        'rate' => $exchange['sell_average_rate'],
                        'volume' => $exchange['sell_volume'],
                        'btc_value' => $exchange['sell_average_rate'] * $exchange['sell_volume'],
                        ];
                }
            }
            if ($minSell['rate'] * self::PROFIT_MARGIN < $maxBuy['rate'] and $minSell['rate'] > 0) {
                $options[$pair] = [
                    'min_sell' => $minSell,
                    'max_buy' => $maxBuy,
                    ];
            }
        }

        return $options;
    }
}
