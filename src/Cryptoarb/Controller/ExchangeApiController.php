<?php

namespace Cryptoarb\Controller;

use Cryptoarb\ExchangeApiInterface;

abstract class ExchangeApiController implements ExchangeApiInterface
{

    protected
        $db,
        $qb,
        $exchange = [],
        $markets = [],
        $mch,
        $separator,
        $urlMarketsVar;

    public function __construct($db)
    {
        $this->db = $db;
        $this->qb = $this->db->createQueryBuilder();
        $this->mch = curl_multi_init();
    }

    public function updateExchange(array $exchange)
    {
        $this->exchange = $exchange;
        $json = $this->getAndSaveMarketsList();
        $this->setMarkets($json);
        $c = count($this->markets);
        ($c < self::MAX_CONNECTIONS) ? $maxConnections = $c : $maxConnections = self::MAX_CONNECTIONS;
        for ($i = 0; $i < $maxConnections; $i++) {
            $this->addHandleToMultiCurl($i);
        }
        $this->multiDownloadStart($i, $c);
        curl_multi_close($this->mch);
    }

    protected function getAndSaveMarketsList()
    {
        if (time() - (new \DateTime($this->exchange['synch_time']))->getTimestamp() > 86400 or $this->exchange['json'] === null) {
            $json = json_decode($this->getUrlContent($this->exchange['markets_url']));
            if ($this->isJsonValid($json)) {
                $this->qb
                    ->update('exchanges')
                    ->set('json', ':json')
                    ->set('synch_time', ':datetime')
                    ->where('id = :id')
                    ->setParameter('json', json_encode($json))
                    ->setParameter('datetime', (new \DateTime())->format('Y-m-d H:i:s'))
                    ->setParameter('id', $this->exchange['id'])
                ;
                $this->qb->execute();
                $this->qb->resetQueryParts();
            }
        } else {
            $json = json_decode($this->exchange['json']);
        }

        return $json;
    }

    protected function getIgnoreCoins()
    {
        $ignoreCoins = json_decode($this->exchange['ignore_coins']);
        if (is_array($ignoreCoins)) {
            $match = '';
            foreach ($ignoreCoins as $coin) {
                $match .= '^' . $coin . '|' . $coin . '$|';
            }

            return '/' . trim($match, '|') . '/i';
        }

        return null;
    }

    abstract protected function isJsonValid($json);

    abstract protected function setMarkets($json);

    protected function addHandleToMultiCurl(&$i)
    {
        $ch = curl_init($this->buildMarketUrl($this->markets[$i]->marketName));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        if ($this instanceof BanxApiController) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'uts: ' . time(),
            ]);
        }
        curl_multi_add_handle($this->mch, $ch);
    }

    protected function buildMarketUrl($marketName)
    {
        if (strpos($this->exchange['orderbook_url'], '$1') !== false) {
            $marketName = str_replace(['_', '/', '\\'], '-', $marketName);
            $coins = explode('-', $marketName);

            return str_replace(['$1', '$2'], $coins, $this->exchange['orderbook_url']);
        } else {
            return $this->exchange['orderbook_url'] . $marketName;
        }
    }

    protected function multiDownloadStart(&$i, &$c)
    {
        $active = null;
        do {
            while (($mrc = curl_multi_exec($this->mch, $active)) === CURLM_CALL_MULTI_PERFORM);
            while ($mchinfo = curl_multi_info_read($this->mch)) {
                $url = curl_getinfo($mchinfo['handle'])['url'];
                $marketName = $this->getMarketNameFromUrl($url);
                $json = json_decode(curl_multi_getcontent($mchinfo['handle']));
                $this->savePairOrderBook($json, $marketName);
                curl_multi_remove_handle($this->mch, $mchinfo['handle']);
                curl_close($mchinfo['handle']);
                if ($i < $c) {
                    $this->addHandleToMultiCurl($i);
                    $i++;
                }
            }
            if ($active && (curl_multi_select($this->mch) === -1)) {
                usleep(100);
            }
        } while ($active);
    }

    protected function getMarketNameFromUrl($url)
    {
        if (is_string($this->urlMarketsVar)) {
            parse_str($url, $vars);

            return $vars[$this->urlMarketsVar];
        } else if (is_array($this->urlMarketsVar)) {
            parse_str($url, $vars);

            return $vars[$this->urlMarketsVar[0]] . $this->separator . $vars[$this->urlMarketsVar[1]];
        } else if ($this->urlMarketsVar === null) {
            return array_pop(explode('/', $url));
        } else {
            throw new \Exception('Invalid urlMarketsVar type');
        }
    }

    protected function savePairOrderBook($json, $marketName)
    {
        $buyData = $this->getBuyData($json);
        $sellData = $this->getSellData($json);
        $coinsAcronyms = $this->getCoinsAcronyms($marketName);
        $this->qb
            ->select('id')
            ->from('coin_pairs')
            ->where('exchange = :exchange')
            ->andWhere('base_coin = :baseCoin')
            ->andWhere('market_coin = :marketCoin')
            ->setParameters([
                'exchange' => $this->exchange['id'],
                'baseCoin' => $coinsAcronyms[0],
                'marketCoin' => $coinsAcronyms[1],
                ])
            ;
        $result = $this->qb->execute()->fetch();
        $this->qb->resetQueryParts();
        if (!$result) {
            $this->qb
                ->insert('coin_pairs')
                ->values([
                    'exchange' => ':exchange',
                    'base_coin' => ':baseCoin',
                    'market_coin' => ':marketCoin',
                    'buy_average_rate' => ':buyAverageRate',
                    'buy_volume' => ':buyVolume',
                    'sell_average_rate' => ':sellAverageRate',
                    'sell_volume' => ':sellVolume',
                ])
                ->setParameters([
                    'exchange' => $this->exchange['id'],
                    'baseCoin' => $coinsAcronyms[0],
                    'marketCoin' => $coinsAcronyms[1],
                    'buyAverageRate' => $buyData['buyAverageRate'],
                    'buyVolume' => $buyData['buyVolume'],
                    'sellAverageRate' => $sellData['sellAverageRate'],
                    'sellVolume' => $sellData['sellVolume'],
                    ])
                ;
            $this->qb->execute();
            $this->qb->resetQueryParts();
        } else {
            $this->qb
                ->update('coin_pairs')
                ->set('buy_average_rate', ':buyAverageRate')
                ->set('buy_volume', ':buyVolume')
                ->set('sell_average_rate', ':sellAverageRate')
                ->set('sell_volume', ':sellVolume')
                ->where('id = :id')
                ->setParameters([
                    'id' => $result['id'],
                    'buyAverageRate' => $buyData['buyAverageRate'],
                    'buyVolume' => $buyData['buyVolume'],
                    'sellAverageRate' => $sellData['sellAverageRate'],
                    'sellVolume' => $sellData['sellVolume'],
                    ])
                ;
            $this->qb->execute();
            $this->qb->resetQueryParts();
        }
    }

    protected function getCoinsAcronyms($marketName)
    {
        $coins = explode($this->separator, strtoupper($marketName));
        if ($coins[0] !== 'BTC') {
            $coins = array_reverse($coins);
        }

        return $coins;
    }

    protected function getUrlContent($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    abstract protected function getBuyData($json);

    abstract protected function getSellData($json);
}
