<?php

namespace Cryptoarb\Controller;

use Cryptoarb\Controller\ExchangeApiController;

class BanxApiController extends ExchangeApiController
{

    protected
        $separator = '/',
        $urlMarketsVar = ['c', 'p'];

    protected function isJsonValid($json)
    {
        return (is_array($json)) ? true : false;
    }

    protected function setMarkets($json)
    {
        $ignore = $this->getIgnoreCoins();
        foreach ($json as $marketName) {
            if (strpos(strtoupper($marketName), 'BTC') !== false
                and ($ignore === null or ($ignore !== null and preg_match($ignore, $marketName) === 0))) {
                $market = new \stdClass();
                $market->marketName = $marketName;
                $this->markets[] = $market;
            }
        }
    }

    protected function getBuyData($json)
    {
        $i = $buyTotalValue = $buyVolume = $buyAverageRate = 0;
        if (isset($json->market->buyorders)) {
            foreach ($json->market->buyorders as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $buyTotalValue += round($order->quantity * $order->price, 8);
                $buyVolume += $order->quantity;
                $i++;
            }
            if ($buyVolume > 0) {
                $buyAverageRate = round($buyTotalValue / $buyVolume, 8);
            }
        }

        return [
            'buyAverageRate' => $buyAverageRate,
            'buyVolume' => $buyVolume,
        ];
    }

    protected function getSellData($json)
    {
        $i = $sellTotalValue = $sellVolume = $sellAverageRate = 0;
        if (isset($json->market->sellorders)) {
            foreach ($json->market->sellorders as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $sellTotalValue += round($order->quantity * $order->price, 8);
                $sellVolume += $order->quantity;
                $i++;
            }
            if ($sellVolume > 0) {
                $sellAverageRate = round($sellTotalValue / $sellVolume, 8);
            }
        }

        return [
            'sellAverageRate' => $sellAverageRate,
            'sellVolume' => $sellVolume,
        ];
    }
}
