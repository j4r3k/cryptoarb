<?php

namespace Cryptoarb\Controller;

use Cryptoarb\Controller\ExchangeApiController;

class PoloniexApiController extends ExchangeApiController
{
    protected
        $separator = '_',
        $urlMarketsVar = 'currencyPair';

    protected function isJsonValid($json)
    {
        return ($json instanceof \stdClass and !isset($json->error)) ? true : false;
    }

    protected function setMarkets($json)
    {
        $ignore = $this->getIgnoreCoins();
        foreach (get_object_vars($json) as $marketName => $market) {
            if ($market->isFrozen === '0' and strpos(strtoupper($marketName), 'BTC') !== false
                and ($ignore === null or ($ignore !== null and preg_match($ignore, $marketName) === 0))) {
                $market->marketName = $marketName;
                $this->markets[] = $market;
            }
        }
    }

    protected function getBuyData($json)
    {
        $i = $buyTotalValue = $buyVolume = $buyAverageRate = 0;
        if (!isset($json->error) and isset($json->bids)) {
            foreach ($json->bids as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $buyTotalValue += round($order[1] * $order[0], 8);
                $buyVolume += $order[1];
                $i++;
            }
            if ($buyVolume > 0) {
                $buyAverageRate = round($buyTotalValue / $buyVolume, 8);
            }
        }

        return [
            'buyAverageRate' => $buyAverageRate,
            'buyVolume' => $buyVolume,
        ];
    }

    protected function getSellData($json)
    {
        $i = $sellTotalValue = $sellVolume = $sellAverageRate = 0;
        if (!isset($json->error) and isset($json->asks)) {
            foreach ($json->asks as $order) {
                if ($i === self::LIMIT) {
                    break;
                }
                $sellTotalValue += round($order[1] * $order[0], 8);
                $sellVolume += $order[1];
                $i++;
            }
            if ($sellVolume > 0) {
                $sellAverageRate = round($sellTotalValue / $sellVolume, 8);
            }
        }

        return [
            'sellAverageRate' => $sellAverageRate,
            'sellVolume' => $sellVolume,
        ];
    }
}
