<?php

namespace Cryptoarb\Factory;

class ExchangeApiFactory
{

    public static function build($db, $exchange)
    {
        $class = 'Cryptoarb\\Controller\\' . $exchange . 'ApiController';
        if (class_exists($class)) {
            return new $class($db);
        }
    }
}
